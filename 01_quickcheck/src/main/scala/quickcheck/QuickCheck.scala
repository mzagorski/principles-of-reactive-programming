package quickcheck

import java.util.Random

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  val rand = new Random()


  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h))==m
  }

  property("min2") = forAll { (a: Int, b: Int) =>
    val h = insert(b, insert(a, empty))
    val min = if(a < b) a else b
    findMin(h) == min
  }

  property("insertDelete") = forAll { a: Int =>
    val h = deleteMin(insert(a, empty))
    isEmpty(h)
  }

  property("meld1") = forAll { (h1: H, h2: H) =>
    val min = Seq(findMin(h1), findMin(h2)).min
    findMin(meld(h1, h2)) == min && findMin(meld(h2, h1)) == min
  }

  private def areEquals(s: Seq[Int], h: H): Boolean = {
    if(s.isEmpty && isEmpty(h)) {
      true
    } else {
      val e = findMin(h)
      s.head == e && areEquals(s.tail, deleteMin(h))
    }
  }

  property("addSeq") = forAll { s: Seq[Int] =>
    val h = s.foldLeft(empty)((h, e) => insert(e, h))
    areEquals(s.sorted, h)
  }

  lazy val genHeap: Gen[H] = for {
    e <- Arbitrary.arbitrary[A]
    h <- oneOf(const(empty), genHeap)
  } yield insert(e, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
